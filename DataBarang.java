package Data;
public class DataBarang {
    //tempat untuk atribut
    String kode;
    String nama;
    float harga;
    int jumlah;
    float total;
    String bonus;
    //tempat untuk method
    public void setData(String kode, String nama, float harga, int jumlah) {
        this.kode = kode;
        this.nama = nama;
        this.harga = harga;
        this.jumlah = jumlah;
        this.total = harga*jumlah;
        getBonus();
    }

    public float getTotal() {
        return total;
    }

    public void getBonus() {
        if (total >= 500000 && jumlah > 5) {
            this.bonus = "Setrika";
        } else if (total >= 100000 && jumlah > 3) {
            this.bonus = "Payung";
        } else if (total >= 50000 || jumlah > 2) {
            this.bonus = "Ballpoint";
        }
    }
        public void printBonus() {
            if (!bonus.equals("")) {
                System.out.println("Bonus yang Anda dapatkan adalah " + bonus);
            } else {
                System.out.println("Maaf, Anda tidak mendapat bonus.");
            }
        }
        
        public String returnBonus() {
            return this.bonus;
        }
    
    public void cetakNota() {
        System.out.println("---------------");
        System.out.println("Kode Barang  : " + kode);
        System.out.println("Nama Barang  : " + nama);
        System.out.println("Harga Barang : Rp. " + harga);
        System.out.println("Jumlah       : " + jumlah);
        System.out.println("Total Harga  : Rp. " + total);
        System.out.println("Bonus        : " + returnBonus());
        System.out.println("----------------");
    }
}
