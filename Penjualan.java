import java.util.Scanner;
import Data.DataBarang;
public class Penjualan {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        char lanjut = 'y';

        while (lanjut == 'y' || lanjut == 'Y') {
            System.out.println("------------------------");
            System.out.print("Masukkan kode barang  : ");
            String kode = input.nextLine();
            System.out.print("Masukkan nama barang  : ");
            String nama = input.nextLine();
            System.out.print("Masukkan harga barang : ");
            float harga = input.nextFloat();
            System.out.print("Masukkan jumlah       : ");
            int jumlah = input.nextInt();
            input.nextLine();
            System.out.println("------------------------");

            DataBarang dataPenjualan = new DataBarang();
            dataPenjualan.setData(kode, nama, harga, jumlah);
            dataPenjualan.cetakNota();

            System.out.print("Input barang lagi [Y/N] ? ");
            lanjut = input.nextLine().charAt(0);
        }

        System.out.println("Terima Kasih Telah Berbelanja.");
        input.close();
    }
}